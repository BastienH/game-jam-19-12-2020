#include <systems/Physics.hpp>
#include <cmath>
#include <components/Rigidbody.hpp>
#include <components/Position.hpp>

namespace systems
{
	Physics::Physics() :
		_gravity(0.f, 9.8f),
		_fixedTimestepAccumulator(0),
		_fixedTimeStepAccumulatorRatio(0)
	{
	}

	Physics::~Physics()
	{
	}

	void Physics::update(float deltaTime, std::shared_ptr<game::World> world)
	{
		auto physicWorld = world->getPhysicWorld();

		_fixedTimestepAccumulator += deltaTime;
		const int nSteps = static_cast<int>(std::floor(_fixedTimestepAccumulator / FIXED_TIMESTEP));

		if (nSteps > 0)
		{
			_fixedTimestepAccumulator -= nSteps * FIXED_TIMESTEP;
		}

		assert(_fixedTimestepAccumulator < FIXED_TIMESTEP + FLT_EPSILON);

		_fixedTimeStepAccumulatorRatio = _fixedTimestepAccumulator / FIXED_TIMESTEP;

		const int nStepsClamped = std::min(nSteps, MAX_STEPS);
		for (int i = 0; i < nStepsClamped; i++)
		{
			resetSmoothStates(physicWorld);
			singleStep(FIXED_TIMESTEP, physicWorld);
		}

		physicWorld->ClearForces();

		smoothStates(physicWorld);
		
		// Update other components
		world->getRegistry()->each<components::Position, components::Shape, components::Rigidbody>([](components::Position& pos, components::Shape& shape, components::Rigidbody& body)
			{
				Vec2 position(body.getRigidbody()->GetPosition());

				pos = position;
				shape.setPosition(position);
				shape.setRotation(body.getRigidbody()->GetAngle() * 180 / b2_pi);
			});
	}

	void Physics::smoothStates(std::shared_ptr<b2World> world)
	{
		const float dt = _fixedTimeStepAccumulatorRatio * FIXED_TIMESTEP;

		for (b2Body * b = world->GetBodyList(); b != nullptr; b = b->GetNext())
		{
			if (b->GetType() == b2_staticBody)
				continue;

			b->SetTransform(b->GetPosition() + dt * b->GetLinearVelocity(), b->GetAngle() + dt * b->GetAngularVelocity());
		}
	}

	void Physics::resetSmoothStates(std::shared_ptr<b2World> world)
	{
		for (b2Body* b = world->GetBodyList(); b != nullptr; b = b->GetNext())
		{
			if (b->GetType() == b2_staticBody)
				continue;

			b->SetTransform(b->GetPosition(), b->GetAngle());
		}
	}

	void Physics::singleStep(float deltaTime, std::shared_ptr<b2World> world)
	{
		world->Step(deltaTime, 8, 3);
	}
}