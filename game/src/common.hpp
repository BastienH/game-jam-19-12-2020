#pragma once

#include <cstdint>
#include <memory>
#include <chrono>
using namespace std::literals::chrono_literals;
#include <thread>
#include <string>
#include <algorithm>

using ObjectID_t = uint64_t;

extern bool FLAG_hitbox_debug;