#include <components/Rigidbody.hpp>

namespace components
{
	Rigidbody::Rigidbody(std::shared_ptr<b2World> world, components::Shape const & shape, Type type)
	{
		b2BodyDef bodyDef;
		bodyDef.position.Set(shape.getPosition().x, shape.getPosition().y);
		if (type == Type::Dynamic)
			bodyDef.type = b2_dynamicBody;

		_rigidbody = world->CreateBody(&bodyDef);

		// Shape
		Vec2 size(shape.getSize());
		_b2Shape.SetAsBox(size.x / 2.f, size.y / 2.f, b2Vec2(size.x / 2.f, size.y / 2.f), _rigidbody->GetAngle());

		// Fixture
		b2FixtureDef fixtureDef;
		fixtureDef.shape = &_b2Shape;
		if (type == Type::Dynamic)
		{
			fixtureDef.density = 1.f;
			fixtureDef.friction = 0.3f;
		}
		else
		{
			fixtureDef.density = 0.f;
		}
		_rigidbody->CreateFixture(&fixtureDef);
	}

	Rigidbody::~Rigidbody()
	{
	}
}