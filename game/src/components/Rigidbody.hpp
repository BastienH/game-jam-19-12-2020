#pragma once

#include <common.hpp>
#include <box2d/box2d.h>

#include <components/Shape.hpp>

namespace components
{
	class Rigidbody
	{
	public:
		enum class Type
		{
			Static,
			Dynamic
		};

		Rigidbody(std::shared_ptr<b2World> world, components::Shape const & shape, Type type = Type::Static);
		~Rigidbody();

		b2Body* getRigidbody() { return _rigidbody; }

	private:
		b2Body* _rigidbody;
		b2PolygonShape _b2Shape;
	};
}