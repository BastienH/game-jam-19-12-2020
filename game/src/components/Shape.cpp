#include <components/Shape.hpp>

namespace components
{
	Shape::Shape(components::Position const& pos, Vec2 const& size) :
		_rectShape(sf::Vector2f(size.x - 2, size.y - 2)),
		_debugOn(true)
	{
		_rectShape.setPosition(sf::Vector2f(pos.x + 1, pos.y + 1));
		_rectShape.setFillColor(sf::Color::Transparent);
		_rectShape.setOutlineColor(sf::Color::Green);
		_rectShape.setOutlineThickness(1);
	}

	Shape::~Shape()
	{
	}

	void Shape::showDebug(bool value)
	{
		_debugOn = value;
	}

	void Shape::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		if (_debugOn)
			target.draw(_rectShape, states);
	}
}