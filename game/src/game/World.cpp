#include <game/World.hpp>
#include <components/Name.hpp>

namespace game
{
	World::World()
	{
		_b2World = std::make_shared<b2World>(b2Vec2(0.f, 9.8f));
		_b2World->SetAutoClearForces(false);

		_registry = std::make_shared<Registry>();
	}

	World::~World()
	{
		_registry = nullptr;
		_b2World = nullptr;
	}

	ObjectID_t World::createGameObject()
	{
		return _registry->createGameObject();
	}
}