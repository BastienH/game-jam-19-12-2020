#include <game/Game.hpp>

#include <systems/Physics.hpp>

#include <game/GameObject.hpp>

#include <components/Shape.hpp>
#include <components/Position.hpp>
#include <components/Rigidbody.hpp>

namespace game
{
	Game::Game() :
		_isRunning(false)
	{
	}

	Game::~Game()
	{
	}

	void Game::startup()
	{
		_window = std::make_unique<sf::RenderWindow>(sf::VideoMode(800, 800), "Game");
		_world = std::make_shared<World>();

		_systems.push_back(std::make_unique<systems::Physics>());

		// Create some entities to test
		std::shared_ptr<GameObject> floor = std::make_shared<GameObject>("Floor", _world);
		auto pos = floor->addComponent<components::Position>(0.f, 600.f);
		auto shape = floor->addComponent<components::Shape>(pos, Vec2(100.f, 10.f));
		floor->addComponent<components::Rigidbody>(_world->getPhysicWorld(), shape);

		std::shared_ptr<GameObject> cube = std::make_shared<GameObject>("Cube", _world);
		auto cPos = cube->addComponent<components::Position>(10.f, 0.f);
		auto cShape = cube->addComponent<components::Shape>(cPos, Vec2(10.f, 10.f));
		cube->addComponent<components::Rigidbody>(_world->getPhysicWorld(), cShape, components::Rigidbody::Type::Dynamic);
	}

	void Game::run()
	{
		if (_isRunning) return;
		_isRunning = true;

		float deltaTime = 0;

		while (_isRunning && _window->isOpen())
		{
			auto start = std::chrono::high_resolution_clock::now();

			sf::Event event;
			while (_window->pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
				{
					stop();
					return;
				}
			}

			// Updates
			for (auto& system : _systems)
			{
				system->update(deltaTime, _world);
			}

			std::this_thread::sleep_for(1ms); // simulate a bit of activity until we have some

			auto end = std::chrono::high_resolution_clock::now();
			deltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

			_window->clear();

			_world->getRegistry()->each<components::Shape>([this](components::Shape& shape)
				{
					_window->draw(shape);
				});

			_window->display();
		}
	}

	void Game::stop()
	{
		_isRunning = false;
		_window->close();
	}
}