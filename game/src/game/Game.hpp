#pragma once

#include <vector>

#include <SFML/Graphics.hpp>

#include <common.hpp>
#include <game/World.hpp>
#include <systems/System.hpp>

namespace game
{
	class Game
	{
	public:
		Game();
		~Game();

		void startup();
		void run();
		void stop();

	private:
		bool _isRunning;

		std::unique_ptr<sf::RenderWindow> _window;
		std::shared_ptr<World> _world;
		std::vector<std::unique_ptr<systems::ISystem>> _systems;
	};
}